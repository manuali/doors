# Manuale Tag Doors

### Indice

1. [Descrizione](#descrizione)
2. [Installazione](#installazione)

### Tabelle di base
- [Parametri](#parametri)
- [Aperture](#aperture)
- [Finiture](@finiture)
- [Battute](#battute)
- [Tipi Telaio](#tipi-telaio)
  
### Documenti
- [Maggiorazioni](#maggiorazioni)
- [Configurazioni](#configurazioni)
- [Politica Telai](#politica-telai)


### Suggerimenti Operativi

- [Caricare pannello](#Caricare-pannello)



  


## Descrizione

Tag Doors è una procedura che ha lo scopo di ottimizzare la configurazione e la produzione di porte.
Si compone di due progetti:

- Progetto Easy Studio contiene tutte le implementazioni  integrate con l'ERP MAGO4

- Progetto Winform Contine la finestra di configurazione prodotto

## Parametri

Nei parametri sono definite tre tab relative a: 
- #### Connessione Magic Link  
  Contiene i parametri necessari a chiamare la funzione di esplosione distinta ed è utilizzata nel configuratore
  
- #### Categorie
  Vengono definite le categorie chiave che servono ad identificare i componenti cardine della porta. In base alle selezioni effettuate nel configuratore sarà necessario identificare, all'interno della struttura ad albero del configuratore alcuni elementi appartenenti alle categorie qui definite.

- #### Contatori
  Attualmente viene utilizzato per definire l'id della configurazione generata.



  

## Politica Telai

Con questo documento viene definita la politica con cui "calcolare" ***la larghezza*** del telaio da utilizzare in base alla dimensione del muro e ***la quantita e il tipo*** di coprifili da utilizzare.

- I dati di testa <br>

|Campo|descrizione 
|---|---|
|Codice|Viene composto automaticamente in base ai valori selezionati|
|Tipo Telaio|Contiene il codice dei tipi di telaio definiti nella tabella. Entra in gioco in fase di configurazione nella selezione delle politiche da applicare. Infatti in ogni configurazione base la ***Distinta del telaio*** ha definito il campo ***Tipo Telaio*** che nel configuratore servirà a selezionare le politiche da applicare alla configurazione.  |
|Descrizione|Viene autocomposta in base ai valori selezionati|
|Spessore Muro da | Limite inferiore spessore muro |
|Spessore Muro a | Limite superiore spessore muro |
|L Telaio| Identifica la larghezza del ***componente telaio*** da utilizzare in configurazione nel caso in cui la dimensione spessore muro indicata in configurazione ricada nel range di spessore definito nei campi precedenti.


# Cosa succede nel configuratore quando cambiamo lo spessore del muro ?? #

### Step 1

1. Il programma controlla che tipo di telaio è stato indicato nella ***distinta telaio*** presente nella distinta base in configurazione (es. 001 - tipo 7+7) 
   
![alt text](img00.jpg "Impostazioni Categorie in un Telaio distinta base")
1. Una volta acquisito il tipo di telaio con cui dobbiamo avere a che fare procede a leggere le politiche che sono associate a quel tipo di telaio  ed a creare un elenco delle politiche

![alt text](img2.jpg "Politica telaio")

1. Dopo avere letto tutte le politiche possibili per quel tipo telaio cerca quale politica applicare scorrendo nell'elenco e selezionando quella in cui lo spessore del muro ricade nel range di misure. 
2. Una volta individuata la politica da applicare seleziona la larghezza del ***componente telaio*** da ricercare tra i telai possibili.
    ### Come viene selezionato il componente telaio da applicare ?? ###
   - fino a questo momento noi siamo in possesso solo della sua larghezza ma, poichè dovremo selezionare un item di magazzino, dovremo eseguire una selezione. 
   - Il primo filtro utile è il ***tipo di telaio*** di cui siamo in possesso (ma serve solo aselezionare la politica). 
   - Il secondo filtro è la ***finitura*** che è la stessa del componente pannello
   - la terza è la categoria merceologica del ***componente telaio*** (definita nei parametri tra le categorie alla voce **"Telai"**) 
   
![alt text](img0.jpg "Impostazioni categorie chiave")

Riassumendo... al momento dell'esplosione distinta viene definita la finitura della porta e viene costruito un elenco di ***componenti telaio*** (da non confondere con le ***distinte telaio***) che su mago appartanegono alla stessa finitura ed alla categoria merceologica dei ***componenti telaio*** definita nei parametri.

Nella figura qui sotto il ***componente telaio*** farà parte dell'elenco dei telai possibili con la finitura 001 MDF 

![alt text](img1.jpg "Impostazioni articolo Componente Telaio")


al momento di sceglire quale componente inserire in automatico verrà scelto dall'elenco quello con la Larghezza espressa nella politica.

### Step 2 
#### Selezione coprifili

Una volta individuata la politica da applicare ed il telaio da inserire viene preso in esame il corpo della politica che serve a definire la larghezza, il tipo e la combinazione di corpifili necessaria.

Riprendiamo la schermata della definizione della politica:

***Fig.1***

![alt text](img2.jpg "Politica telaio")

Digitando spessore Muro cm 14 verrà selezionata la politica definita in figura 1 e dopo avere stabilito il telaio andrema a selezionare la riga 2 (14cm sono compresi tra 13,50 e 14,50) e dovremo inserire nella distinta telaio 2,50 coprifili da 7 e 2,50 coprifili da 7 "aletta lunga".

### Come vengono selezionati i coprifili ?? ###

In fase di esplosione distinta viene creata una lista di coprifili presenti nella configurazione originale della distinta. In particolare vengono aggiunti alla lista tutti i componenti che appartengono alle 2 categorie di coprifili definite nei parametri: Coprifili e Coprifili Aletta lunga


***Fig.2***

![alt text](img3.jpg "Parametri - categorie coprifili")

Questa lista serve a stabilire i coprifili presenti ma soprattutto a definire il ***Profilo Coprifili***. Questo elemento serve ad selezionare automaticamente un coprifilo retto o bombato.
In anagrafica articolo per definire se si tratta di un coprifilo bombato o retto va definito il campo "Categoria Omogenea".



***Fig.3 Anagrafica articolo coprifilo normale finitura MDF bombato da 9 cm:***

![alt text](img4.jpg "Anagrafica coprifilo")


***Fig.4 Anagrafica articolo coprifilo Aletta lunga finitura MDF bombato da 7 cm:***

![alt text](img5.jpg "Anagrafica coprifilo")


Quindi a parità di profilo (Categoria Omogenea di Mago) Categoria (coprifilo normale o aletta lunga), larghezza (da 7 o da 9), e finitura verranno inseriti/eliminati i ***componenti coprifilo*** definiti nella politica.



## Caricare pannello

Nel definire la distinta base di una porta, al fine di consentire il corretto funzionamento in fase di configuratore dobbiamo fare attenzione ai seguenti elementi:

***Categoria merceologica articoli***

Nell'anagrafica articolo (nella sezione ***altri dati***) vanno associati alla categoria giusta i due elementi chiave del pannello che i seguenti:
- ***distinta del pannello***  Da associare alla categoria **"Pannelli lavorati"** definita nei parametri
- ***componente pannello*** Da associare alla categoria **"Pannelli"** definita nei parametri

***Dimensioni***

Nell'anagrafica articolo (nella sezione ***altri dati***) vanno specificate le dimensioni del componente pannello (non serve specificarle nella distinta pannello)

- ***componente pannello*** 
  
   Fondamentale definire larghezza ed altezza (verranno utilizzate al cambio larghezza nel configuratore)
   Fondamentale definire la finitura (verrà utilizzata per proporre i componenti pannello "fratelli" del componente base definito in distinta)

